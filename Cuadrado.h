#ifndef CUADRADO_H
#define CUADRADO_H

#include "Punto2D.h"
#include "Transformacion.h"

class Cuadrado {
public:
    Cuadrado();
    Cuadrado(const Cuadrado& orig);
    virtual ~Cuadrado();
    void crearCuadrado(Punto2D &a, Punto2D &b);
    void dibujarCuadrado();
    void asignarColor(float, float, float);
    void mover(float, float);
    void girar(float);
    void escalar(float, float);
    void origenP();
    bool seleccionada;
    
    float getX();
    float getY();
    
    void dibujarPoligono4l();
    void crearPoligono4l(Punto2D &a, Punto2D &b, Punto2D &c, Punto2D &d);
    
    void crearTriangulo(Punto2D &a, Punto2D &b, Punto2D &c);
    void dibujarTriangulo();
    
private:
    Punto2D p1;
    Punto2D p2;
    Punto2D p3;
    Punto2D p4;
    
    Punto2D a;
    Punto2D bp;
    
    Punto2D pt1;
    Punto2D pt2;
    Punto2D pt3;
    
    Punto2D pp1;
    Punto2D pp2;
    Punto2D pp3;
    Punto2D pp4;
    float r, g, b;
};

#endif /* CUADRADO_H */

