#include "Cuadrado.h"
#include <GL/freeglut.h>
//#include "Transformacion.h"
#include <stdio.h>

Cuadrado::Cuadrado() {
    r = 1; 
    g = 1; 
    b = 0;
}

Cuadrado::Cuadrado(const Cuadrado& orig) {
}

Cuadrado::~Cuadrado() {
}

float Cuadrado::getX(){
    return float(p1.x);
}

float Cuadrado::getY(){
    return float(p1.y);
}

void Cuadrado::crearCuadrado(Punto2D& a, Punto2D& b){
    p1.x = a.x;
    p1.y = a.y;
    
    p2.x = a.x;
    p2.y = b.y;
    
    p3.x = b.x;
    p3.y = b.y;
    
    p4.x = b.x;
    p4.y = a.y;
}

void Cuadrado::dibujarCuadrado(){
    glColor3f(r, g, b);
    if(seleccionada){
        glColor3f(0, 1, 1);
    }else{
        glColor3f(r, g, b);
    }
    glBegin(GL_QUADS);
        glVertex2f(p1.x, p1.y);
        glVertex2f(p2.x, p2.y);
        glVertex2f(p3.x, p3.y);
        glVertex2f(p4.x, p4.y);
    glEnd();
}

void Cuadrado::asignarColor(float red, float green, float blue){
    r = red;
    g = green;
    b = blue;
}

void Cuadrado::mover(float tx, float ty){
    Transformacion t;
    t.traslacion(p1, tx, ty);
    t.traslacion(p2, tx, ty);
    t.traslacion(p3, tx, ty);
    t.traslacion(p4, tx, ty);
}

void Cuadrado::girar(float grados){
    Transformacion t;
    t.rotacion(p1, p1, grados);
    t.rotacion(p2, p1, grados);
    t.rotacion(p3, p1, grados);
    t.rotacion(p4, p1, grados);
}
//void Cuadrado::origenP(){
//    Transformacion op;
//    op.origen(p3.x, p1.x, p3.y, p1.y);
//}
void Cuadrado::escalar(float ex, float ey){
    Transformacion te;
    //te.origen(p3.x, p1.x, p3.y, p1.y);
    float xf = ((p3.x-p1.x)/2) + p1.x;
    float yf = ((p3.y-p1.y)/2) + p1.y;
    te.escalacion(p1, ex, ey, xf, yf);
    te.escalacion(p2, ex, ey, xf, yf);
    te.escalacion(p3, ex, ey, xf, yf);
    te.escalacion(p4, ex, ey, xf, yf);
}

void Cuadrado::crearTriangulo(Punto2D& a, Punto2D& b, Punto2D& c){
//    a.x = -150;
//    a.y = 50;
//    b.x = 150;
//    b.y = 50;
//    c.x =  
    p1.x = a.x;
    p1.y = a.y;
    
    p2.x = b.x;
    p2.y = b.y;
    
    p3.x = c.x;
    p3.y = c.y;
    
//    p3.x = c.x;
//    p3.y = c.y;

}

void Cuadrado::dibujarTriangulo(){
    glColor3f(r, g, b);
    if(seleccionada){
        glColor3f(0, 1, 1);
    }else{
        glColor3f(r, g, b);
    }
    glBegin(GL_TRIANGLES);//TRIANGLES);
        glVertex2f(p1.x, p1.y);
        glVertex2f(p2.x, p2.y);
        glVertex2f(p3.x, p3.y);
   glEnd();
}

void Cuadrado::crearPoligono4l(Punto2D& a, Punto2D& b, Punto2D& c, Punto2D& d){
    p1.x = a.x;
    p1.y = a.y;
    
    p2.x = b.x;
    p2.y = b.y;
    
    p3.x = c.x;
    p3.y = c.y;
    
    p4.x = d.x;
    p4.y = d.y;
}

void Cuadrado::dibujarPoligono4l(){
     glColor3f(r, g, b);
    if(seleccionada){
        glColor3f(0, 1, 1);
    }else{
        glColor3f(r, g, b);
    }
    glBegin(GL_POLYGON);
        glVertex2f(p1.x, p1.y);
        glVertex2f(p2.x, p2.y);
        glVertex2f(p3.x, p3.y);
        glVertex2f(p4.x, p4.y);
   glEnd();
}