#aquí van los archivos fuente del proyecto separados con \ y enter
SRC_FILES = main.cpp Cuadrado.cpp Punto2D.cpp Transformacion.cpp Figuras.cpp Triangulo.cpp

INC_DIRS += /usr/include \
	    /usr/local/include/opencv \
	    /usr/local/include

LIB_DIR = /usr/local/lib

USED_LIBS += GL GLU GLEW glut m pthread #Xi X11 
#ml XnVNite_1_4_2
#-L/usr/local/lib -lGL -lglut -lGLU -lm `pkg-config --libs opencv`

BIN_DIR = ./dist/Debug/GNU-Linux

#aquí va el nombre del proyecto, todo en minúsculas
EXE_NAME = tangram

LDFLAGS = -pthread -lrt -lglib-2.0 -L/usr/X11R6/lib -L/usr/local/lib -L$(LIB_DIR)
CFLAGS = -O -pthread -I/usr/local/include/opencv -I/usr/local/include -I/usr/include/gstreamer-0.10 -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -I/usr/include/libxml2 -I/usr/X11R6/include -g -I$(INC_DIR)

include ./CommonMakefile
