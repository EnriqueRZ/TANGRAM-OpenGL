#ifndef TRIANGULO_H
#define TRIANGULO_H
#include "Punto2D.h"
#include "Transformacion.h"

class Triangulo {
public:
    Triangulo();
    Triangulo(const Triangulo& orig);
    virtual ~Triangulo();
    void crearTriangulo(Punto2D &a, Punto2D &b);//, Punto2D &c);
    void dibujarTriangulo();
    void asignarColor(float, float, float);
    void mover(float, float);
    void girar(float);
    void escalar(float, float);
    void origenP();
    bool seleccionada;
    
private:
    Punto2D p1;
    Punto2D p2;
    Punto2D p3;
    float r, g, b;
};

#endif /* TRIANGULO_H */

