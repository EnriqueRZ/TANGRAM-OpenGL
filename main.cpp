#include <cstdlib>
#include <GL/freeglut.h>
#include "Punto2D.h"
#include "Cuadrado.h"
#include <stdio.h>
#include "Figuras.h"
#include "Triangulo.h"
#include <string.h>
#include <strstream>

#include <string>  
#include <iostream>    
#include <sstream>  


int width = 1100;
int height = 700;
Punto2D p1;
Punto2D p2;

Punto2D pt1;
Punto2D pt2;
Punto2D pt3;

Punto2D pp1;
Punto2D pp2;
Punto2D pp3;

Punto2D pl1;
Punto2D pl2;
Punto2D pl3;

Punto2D pc1;
Punto2D pc2;
Punto2D pc3;
Punto2D pc4;

Cuadrado c[100];
Figuras f[100];
Triangulo t[100];
int cTria = 0;
int cFiguras = 0;
int contCuadra = 0;
float r, g, b;
int transformacion;
char *texto;
int selected;
int nivel = 0;
int menu;
int opcion;
int idVentana;
float xS, yS;
int co = 0;
int puntaje1 = 0;
int puntaje2 = 0;
char *pun;

void initCallBacks();
void DisplayRender(void);
void Reshape(int, int);
void Mouse(int button, int action, int x, int y);
void Keyboard(unsigned char key, int x, int y);
void SpecKey(int key, int x, int y);
void menuNivel(int opcion);
void createMenu(void);
void niveles(int nivel);
void glutDestroyWindow(int idVentana);
void getSelected(int selected);
void glWrite(float, float, int, int, int, void *font, char);

using namespace std;

void glWrite(float x, float y, int r, int g, int b, void *font,  char *texto){
    glColor3f(r, g, b);
    glRasterPos2d(x, y);
    for(int i = 0; i < strlen(texto); i++){
        glutBitmapCharacter(font, texto[i]);
    }
}

void getSelected(int selected){
    switch(selected){
        case 0: 
            xS = -150.0; yS = 50.0;
            break;
        case 1: 
            xS = -150.0; yS = 50.0 ;
            break;
        case 2:
            xS = -200.0; yS = 200.0 ;
            break;
        case 3:
            xS = 200.0; yS = 0.0 ;
            break;
        case 4:
            xS = -200.0; yS = 0.0 ;
            break;
        case 5:
            xS = 0.0; yS = 0.0 ;
            break;
        case 6:
            xS = 100; yS = -100.0 ;
            break;
        
    }
}

void SpecKey(int key, int x, int y){
    switch(key){
        case GLUT_KEY_F1:
            transformacion = 1;
            break;
        case GLUT_KEY_F2:
            transformacion = 2;
            break; 
    }
    if(transformacion == 1){
        switch(key){
            case GLUT_KEY_LEFT:
                c[selected].mover(-5, 0);
                break;
            case GLUT_KEY_RIGHT:
                c[selected].mover(5, 0);
                break;
            case GLUT_KEY_DOWN:
                c[selected].mover(0, -5);
                break;
            case GLUT_KEY_UP:
                c[selected].mover(0, 5);
                break;      
        }
    }else if(transformacion == 2){
        switch(key){
            case GLUT_KEY_UP:
                c[selected].girar(5);
                break;
            case GLUT_KEY_DOWN:
                c[selected].girar(-5);
                break;  
        }
    }
    
    glutPostRedisplay();
}
    
void KeyBoard(unsigned char key, int x, int y){ 
    switch(key){
        case '+':
            if(selected < contCuadra-1){
                c[selected].seleccionada = false;
                selected++;
                c[selected].seleccionada = true;
            }
            break;
        case '-':
            if(selected > 0){
                c[selected].seleccionada = false;
                selected--;
                c[selected].seleccionada = true;
            }
            break;   
    }
    
    glutPostRedisplay();
}

void Mouse(int button, int action, int x, int y){

    if(nivel == 1){
        p1.x = -500;
        p1.y = 300;
        p2.x = -200;
        p2.y = 100;
        c[contCuadra].crearCuadrado(p1, p2);
        c[contCuadra].asignarColor(r, g, b);
        contCuadra++;

        pt1.x = 200;
        pt1.y = -200;
        
        pt2.x = -100;
        pt2.y = -200;
        
        pt3.x = 50;
        pt3.y = -350;
        c[contCuadra].crearTriangulo(pt1, pt2, pt3);
        c[contCuadra].asignarColor(r, g, b);
        c[contCuadra].seleccionada = true;
        c[selected].seleccionada = false;
        contCuadra++;
                
        glutPostRedisplay();
    }
    if(nivel == 2){
        p1.x = -500;//-200;
        p1.y = 100;
        
        p2.x = -300;
        p2.y = -300;
        c[contCuadra].crearCuadrado(p1, p2);
        c[contCuadra].asignarColor(r, g, b);
        contCuadra++;

        pt1.x = 500;//-200;
        pt1.y = 150; //0

        pt2.x = 300;//0;
        pt2.y = 150;//0;
        
        pt3.x = 500;//-200;
        pt3.y = -50;//-200;
        c[contCuadra].crearTriangulo(pt1, pt2, pt3);
        c[contCuadra].asignarColor(r, g, b);
        contCuadra++;
        
        pp1.x = -450;//200;
        pp1.y = 120;//0;
        
        pp2.x = -250;//0;
        pp2.y = 120;//0;
        
        pp3.x = -450;//200;
        pp3.y = 320;//-200;
        c[contCuadra].crearTriangulo(pp1, pp2, pp3);
        c[contCuadra].asignarColor(r, g, b);
        contCuadra++;
        
        pl1.x = 0;//0;
        pl1.y = -220;//0;
        
        pl2.x = 100;//100;
        pl2.y = -320;//-100;
        
        pl3.x = -100;//-100;
        pl3.y = -320;//-100;
        c[contCuadra].crearTriangulo(pl1, pl2, pl3);
        c[contCuadra].asignarColor(r, g, b);
        contCuadra++;
        
        pc1.x = 500;//100;
        pc1.y = -150;//-100;
        
        pc2.x = 300;//-100;
        pc2.y = -150;//-100;
        
        pc3.x = 200;//-200;
        pc3.y = -250;//-200;
        
        pc4.x = 600;//200;
        pc4.y = -250;//-200;
        c[contCuadra].crearPoligono4l(pc1, pc2, pc3, pc4);
        c[contCuadra].asignarColor(r, g, b);
        
        c[contCuadra].seleccionada = true;
        c[selected].seleccionada = false;
        contCuadra++;
        glutPostRedisplay();
    }
    
}

void Reshape(int w, int h){
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D((w/2)-w, (w/2), (h/2)-h, (h/2));
    width = w;
    height = h;
}

void DisplayRender(void){
    if(nivel == 0)
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    if(nivel == 1)
        glClearColor(1.0f, 0.0f, 1.0f, 0.0f);
    if(nivel == 2)
        glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);   
    
    if(nivel == 1){
        glWrite(-50, (height/2) -60, 1, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "NIVEL 1");
        
        c[0].dibujarCuadrado();

        c[1].dibujarTriangulo();
        
        f[cFiguras].poligono();
        
    }
    if(nivel == 2){
        glWrite(-50, (height/2) -60, 1, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "NIVEL 2");
 
        c[2].dibujarCuadrado();
        
        c[3].dibujarTriangulo();
        
        c[4].dibujarTriangulo();
        
        c[5].dibujarTriangulo();
        
        c[6].dibujarPoligono4l();
        
        f[cFiguras].poligono2();
               
    }
    
    glWrite(-100, (height/2) -35, 1, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "-TANGRAM MQS-");
    glWrite(400, -340, 1, 0, 0, GLUT_BITMAP_8_BY_13, "STUDIO MQSoftware");

    if(nivel == 0){
        glWrite(-300, 150, 1, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24,   "************************************************");
        glWrite(-300, 120, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24, "*--------------BIENVENIDOS--------------*");
        glWrite(-300, 90, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24,  "*-------------INSTRUCCIONES-------------*");
        glWrite(-300, 50, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24,  "*--1.-DESPLEGAR MENU CON CLICK DERECHO-*");
        glWrite(-300, 20, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24,  "*--2.-SELECCIONAR NIVEL------------------*");
        glWrite(-300, -10, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24,  "*--3.-PRESIONAR F1 PARA MOVER PIEZA------*");
        glWrite(-300, -40, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24,   "*--4.-PRESIONAR F2 PARA GIRAR PIEZA-------*");
        glWrite(-300, -70, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24, "*--5.-MANIPULAR PIEZA CON CURSORES DE---*");
        glWrite(-300, -100, 1, 0, 1, GLUT_BITMAP_TIMES_ROMAN_24, "*-----DESPLAZAMIENTO-------------------*");
        glWrite(-300, -130, 1, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24,   "************************************************");
        
    }
    
    
    getSelected(selected);
    if(c[selected].getX() == xS && c[selected].getY() == yS){
        glWrite(-50, (height/2) -100, 0, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "¡CORRECTO!");
//        
//        if(nivel == 1){
//            puntaje1++;
//            if(puntaje1 == 2){
//                glWrite(-400, -340, 0, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "PUNTAJE PERFECTO");
//            }
//        }
//        if(nivel == 2){
//            puntaje2++;
//            if(puntaje2 == 5){
//                glWrite(-400, -340, 0, 0, 0, GLUT_BITMAP_TIMES_ROMAN_24, "PUNTAJE PERFECTO");
//            }
//        }
        
    }
    
    glutSwapBuffers();
}

void initCallBacks(){
    
    glutDisplayFunc(DisplayRender);
    
    createMenu();
  
    glutReshapeFunc(Reshape);
    glutMouseFunc(Mouse);
    glutKeyboardFunc(KeyBoard);
    glutSpecialFunc(SpecKey);
   
}

void menuNivel(int opcion){
    if(opcion == 0)
        nivel = 0;
    if(opcion == 1)
        nivel = 1;
    if(opcion == 2)
        nivel = 2;
    if(opcion == 3){
        idVentana = glutGetWindow();
        glutDestroyWindow(idVentana);
    }
    glutPostRedisplay();
}

void createMenu(void){
    menu = glutCreateMenu(menuNivel);
    glutAddMenuEntry("INSTRUCCIONES", 0);
    glutAddMenuEntry("Nivel 1", 1);
    glutAddMenuEntry("Nivel 2", 2);
    glutAddMenuEntry("Salir", 3);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(0,0);
    glutCreateWindow("TRANGRAM");
    glClearColor(255, 255, 255, 255);
  
    initCallBacks();
    
    glutMainLoop();
    
    return 0;
}

