#ifndef TRANSFORMACION_H
#define TRANSFORMACION_H
#include "Punto2D.h"
#include <math.h>
#define pi 3.14159

class Transformacion {
public:
    Transformacion();
    Transformacion(const Transformacion& orig);
    virtual ~Transformacion();
    void traslacion(Punto2D&, float, float);
    void rotacion(Punto2D&, Punto2D&, float);
    void escalacion(Punto2D&, Punto2D&, float, float);
    void escalacion(Punto2D&, float, float, float, float);
    void origen(Punto2D&, Punto2D&, Punto2D&, Punto2D&);
    
private:
    float xf, yf;
};

#endif /* TRANSFORMACION_H */

