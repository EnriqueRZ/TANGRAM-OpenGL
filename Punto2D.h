/* 
 * File:   Punto2D.h
 * Author: rocker
 *
 * Created on 7 de marzo de 2018, 09:42 AM
 */

#ifndef PUNTO2D_H
#define PUNTO2D_H

class Punto2D {
public:
    Punto2D();
    Punto2D(const Punto2D& orig);
    virtual ~Punto2D();
    
    float x, y;
    void returnXY();
    
private:

};

#endif /* PUNTO2D_H */

