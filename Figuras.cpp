
#include "Figuras.h"
#include <GL/freeglut.h>
#include <stdio.h>
#include "Triangulo.h"
#include "Cuadrado.h"

//Punto2D vertC[4] = { {201, 101}, {201, -201}, {600, 101}, {601, -200} };

Figuras::Figuras() {

}

Figuras::Figuras(const Figuras& orig) {
}

Figuras::~Figuras() {
}

void Figuras::poligono(){
    glColor3f(0,0,0);
    glBegin(GL_LINE_LOOP);
        glVertex2f(-150, 50);
        glVertex2f(0, 200);
        glVertex2f(150, 50);
        glVertex2f(150, -150);
        glVertex2f(-150, -150);
    glEnd();
}

void Figuras::poligono2(){
    glColor3f(0,0,0);
    glBegin(GL_LINE_LOOP);
        glVertex2f(-200, 200);
        glVertex2f(200, 200);
        glVertex2f(200, -200);
        glVertex2f(-200, -200);
    glEnd();
}

void Figuras::mover(float xm, float ym, float zm){
    glTranslatef(xm, ym, zm);
}

void Figuras::girar(float ag, float yg, float zg, float xg){
    glRotatef(ag, yg, zg, xg);
}

void Figuras::escalar(float xe, float ye){

}



