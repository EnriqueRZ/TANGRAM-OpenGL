
#include "Triangulo.h"
#include <GL/freeglut.h>

Triangulo::Triangulo() {
    r = 1; g = 7; b = 0;
}

Triangulo::Triangulo(const Triangulo& orig) {
}

Triangulo::~Triangulo() {
}

void Triangulo::crearTriangulo(Punto2D& a, Punto2D& b){//, Punto2D& c){
    p1.x = a.x;
    p1.y = a.y;
    
    p2.x = b.x;
    p2.y = b.y;
    
    p3.x = a.x;
    p3.y = b.y;
    
//    p3.x = c.x;
//    p3.y = c.y;

}

void Triangulo::dibujarTriangulo(){
    glColor3f(r, g, b);
    if(seleccionada){
        glColor3f(0, 7, 1);
    }else{
        glColor3f(r, g, b);
    }
    glBegin(GL_TRIANGLES);
        glVertex2f(p1.x, p1.y);
        glVertex2f(p2.x, p2.y);
        glVertex2f(p3.x, p3.y);
   glEnd();
}

void Triangulo::asignarColor(float red, float green, float blue){
    r = red; g = green; b = blue;
}

void Triangulo::escalar(float tx, float ty){
    Transformacion t;
    t.traslacion(p1, tx, ty);
    t.traslacion(p2, tx, ty);
    t.traslacion(p3, tx, ty);
}

void Triangulo::girar(float grados){
    Transformacion t;
    t.rotacion(p1, p1, grados);
    t.rotacion(p2, p1, grados);
    t.rotacion(p3, p1, grados);
}

void Triangulo::mover(float tx, float ty){
    Transformacion t;
    t.traslacion(p1, tx, ty);
    t.traslacion(p2, tx, ty);
    t.traslacion(p3, tx, ty);
}




