#include "Transformacion.h"
#include "Punto2D.h"

Transformacion::Transformacion() {
}

Transformacion::Transformacion(const Transformacion& orig) {
}

Transformacion::~Transformacion() {
}

void Transformacion::traslacion(Punto2D& p, float tx, float ty){
    p.x += tx;
    p.y += ty;
}

void Transformacion::rotacion(Punto2D& po,  Punto2D& pv, float grados){
    float xp, yp;
    float angulo = grados*(pi/180);
    xp = pv.x + ((po.x-pv.x) * cos(angulo)) - ((po.y - pv.y) * sin(angulo));
    yp = pv.y + ((po.x-pv.x) * sin(angulo)) + ((po.y - pv.y) * cos(angulo));
    po.x = xp;
    po.y = yp;
}

void origen(Punto2D& px, Punto2D& ppx, Punto2D& py, Punto2D& ppy){
     float xf = ((px.x-ppx.x)/2) + ppx.x;
     float yf = ((py.y-ppy.y)/2) + ppy.y;
}

//void Transformacion::escalacion(Punto2D& p, Punto2D& pp, float ex, float ey){
void Transformacion::escalacion(Punto2D& p, float ex, float ey, float xf, float yf){
    p.x = (p.x*ex) + xf*(1-ex);  
    p.y = (p.y*ey) + yf*(1-ey);
}
