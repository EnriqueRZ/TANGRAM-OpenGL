/* 
 * File:   Figuras.h
 * Author: rocker
 *
 * Created on 24 de marzo de 2018, 11:10 AM
 */

#ifndef FIGURAS_H
#define FIGURAS_H
#include "Punto2D.h"

class Figuras {
public:
    Figuras();
    Figuras(const Figuras& orig);
    virtual ~Figuras();
    void triangulo();
    void cuadrado();
    void rectangulo();
    void circulo();
    void poligono();
    void poligono2();
    bool seleccionada;
    void mover(float xm, float ym, float zm);
    void girar(float ag, float yg, float zg, float xg);
    void escalar(float, float);
    
    Punto2D vertC[4];
    Punto2D vertT[3];
    
private:

};

#endif /* FIGURAS_H */

